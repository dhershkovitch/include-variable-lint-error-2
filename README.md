# include-variable-lint-error

This project contains information about a bug in the GitLab CI Linter

## Getting started

The `main` branch shows the issue with the included CI files. See issue at https://gitlab.com/gitlab-org/gitlab/-/issues/350676
